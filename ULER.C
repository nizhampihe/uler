/*-=----=----=----=----=----=-*
 *   __   __                  *
 *  / /  / /\___  __          *
 * / /  / / / _ \/*_\         *
 * \ \_/ / / /__/ /           *
 *  \___/_/\___/ /            *
 * /_ \_/  \/ __/             *
 *   \___/\__/ v1.0.0         *
 *                            *
 * ULER.C                     *
 *   Program game sederhana   *
 *                            *
 *        (c) 2018 NizhamPihe *
 *-=----=----=----=----=----=-*/

int angka (char *) ;
void uler (int, int, int) ;

int main (int   argc,
          char *argv[])
{
  int a = 15, b = 15 ;
  if (argc > 1)
  {
    a = angka (argv [1]) ;
    a = (a < 11) ?  11 : (
      (a > 30) ? 30 : a) ; 
    if (argc > 2)
    {
      b = angka (argv [2]) ;
      b = (b < 11) ?  11 : (
        (b > 30) ? 30 : b) ;
    }
    else b = a ;
  }
  uler (3, a, b) ;
  return 0 ;
}

/*-=----=----=----=----=----=-*/

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>

typedef struct _uler ulr ;

typedef enum
{
  item = 0,
  biru = 9,
  ijo,
  cyan,
  merah,
  ungu,
  kuning,
  putih
} wrn ;

typedef enum
{
  awal,
  maen,
  pause,
  gameover,
  gon
} mde ;

typedef enum
{
  kanan,
  kiri,
  atas,
  bawah
} arh ;

struct _uler
{
  arh  arah ;
  int  x ;
  int  y ;
  ulr *next ;
} ;

/*-=----=----=----=----=----=-*/

int angka (char *str)
{
  int i = 0 ;
  int a = 0 ;
  while (str [i] != 0)
  {
    a *= 10 ;
    a += str [i] - 48 ;
    i++ ;
  }
  return a ;
}

void warna (int c)
{
  SetConsoleTextAttribute (
    GetStdHandle (
      STD_OUTPUT_HANDLE
    ), c
  ) ;
}

void up (int h)
{
  COORD loc ;
  CONSOLE_SCREEN_BUFFER_INFO
    cbsi ;
  GetConsoleScreenBufferInfo (
    GetStdHandle (
      STD_OUTPUT_HANDLE
    ), &cbsi
  ) ;
  loc.X = 0 ;
  loc.Y =
  cbsi.dwCursorPosition.Y - h ;
  SetConsoleCursorPosition (
    GetStdHandle (
      STD_OUTPUT_HANDLE
    ), loc
  ) ;
}

int inisiasi ()
{
  CONSOLE_CURSOR_INFO cinfo ;
  CONSOLE_SCREEN_BUFFER_INFO
    cbsi ;
  
  GetConsoleScreenBufferInfo (
    GetStdHandle (
      STD_OUTPUT_HANDLE
    ), &cbsi
  ) ;
	GetConsoleCursorInfo (
	  GetStdHandle (
	    STD_OUTPUT_HANDLE
	  ), &cinfo
	) ;
	
	cinfo.bVisible = FALSE ;

  SetConsoleTitle (
    "ULER.EXE"
  ) ;
	SetConsoleCursorInfo (
	  GetStdHandle (
	    STD_OUTPUT_HANDLE
	  ), &cinfo
	) ;
	
	return cbsi.wAttributes ;
}

void terminasi (int c)
{
  CONSOLE_CURSOR_INFO cinfo ;
  
	GetConsoleCursorInfo (
	  GetStdHandle (
	    STD_OUTPUT_HANDLE
	  ), &cinfo
	) ;
	
	cinfo.bVisible = TRUE ;
	
	SetConsoleCursorInfo (
	  GetStdHandle (
	    STD_OUTPUT_HANDLE
	  ), &cinfo
	) ;
	
	warna (c) ;
	getch () ;
}

/*-=----=----=----=----=----=-*/

void cetak (wrn *papan,
            int  score,
            int  h,
            int  w,
            int  c,
            mde  mode)
{
  int i, j, a, b, d = 4;
  int e = c >> 4 ;
  char scr[] = "00000" ;
  
  warna (11 | e << 4) ;
  printf ("Score") ;
  warna (13 | e << 4) ;
  printf (" : ") ;
  warna (14 | e << 4) ;
  while (score > 0)
  {
    scr [d] = score % 10 + 48 ;
    score /= 10 ;
    d-- ;
  }
  printf ("%s\n", scr) ;
  
  for (i = 0 ; i < h ; i++)
  {
    for (j = 0 ; j < w ; j++)
    {
      if (i % 2)
      {
        a = papan [j + i * w] ;
        b = papan [j + (i - 1)
        * w] ;
        if (
          mode == gameover &&
          (i == h / 2 ||
          i == h / 2 - 1) &&
          j == w / 2 - 4
        )
        {
          warna (14) ;
          printf ("Game Over") ;
          j += 8 ;
        }
        else if (
          mode == pause &&
          (i == h / 2 ||
          i == h / 2 - 1) &&
          j == w / 2 - 2
        )
        {
          warna (6) ;
          printf ("Pause") ;
          j += 4 ;
        }
        else
        {
          warna (b | a << 4) ;
          printf ("%c", 223) ;
        }
      }
    }
    if (i % 2)
      printf ("\n") ;
    if (h % 2 && i == h -1)
    {
      for (j = 0 ; j < w ; j++)
      {
        a = papan [j + i * w] ;
        warna (a | e << 4) ;
        printf ("%c", 223) ;
      }
      printf ("\n") ;
    }
  }
  
  switch (mode)
  {
    case awal :
      warna (11 | e << 4) ;
      printf ("Panah") ;
      warna (13 | e << 4) ;
      printf (":") ;
      warna (11 | e << 4) ;
      printf ("Jalan") ;
      break ;
    case maen :
      warna (11 | e << 4) ;
      printf ("P") ;
      warna (13 | e << 4) ;
      printf (":") ;
      warna (11 | e << 4) ;
      printf ("Pause") ;
      warna (9 | e << 4) ;
      printf (" | ") ;
      warna (11 | e << 4) ;
      printf ("Esc") ;
      warna (13 | e << 4) ;
      printf (":") ;
      warna (11 | e << 4) ;
      printf ("Keluar") ;
      break ;
    case pause :
    case gameover :
      warna (11 | e << 4) ;
      printf (
        "Press any key . . . "
      ) ;
      break ;
  }
}

wrn *plot (ulr *head,
           int *buah,
           int  h,
           int  w)
{
  int i, j ;

  wrn *papan = (wrn *)
    malloc (
      sizeof (wrn) * w * h
    ) ;

  for (i = 0 ; i < h * w ; i++)
    papan [i] = item ;
  for (i = 0 ; i < h ; i++)
    for (j = 0 ; j < w ; j++)
    {
      if (
        j == 0 || j == w - 1 ||
        i == 0 || i == h - 1
      )
        papan [j + i * w]
          = putih ;
    }

  ulr *temp = head ;
  while (temp != NULL)
  {
    papan [temp->x + temp->y
    * w] = ijo ; 
    temp = temp->next ;
  }
  
  papan [buah [0] + buah [1] *
    w] = merah ;
  
  return papan ;
}

void jalan (ulr *head)
{
  ulr *temp = head ;
  while (temp != NULL)
  {
    switch (temp->arah)
    {
      case kanan :
        temp->x++ ;
        break ;
      case kiri :
        temp->x-- ;
        break ;
      case atas :
        temp->y-- ;
        break ;
      case bawah :
        temp->y++ ;
        break ;
    }
    if (temp->next != NULL)
      temp->arah = 
        temp->next->arah ;
    temp = temp->next ;
  }
}

int *berbuah (ulr *head,
              int  h,
              int  w)
{
  srand (time (NULL)) ;
  int *buah = (int *)
    malloc (sizeof (int) * 2 ) ;
  buah [0] =
    rand () % (w - 2) + 1 ;
  buah [1] = 
    rand () % (h - 2) + 1 ;
  ulr *temp = head ;
  while (temp != NULL)
  {
    if (
      temp->x == buah [0] &&
      temp->y == buah [1]
    )
    {
      buah [0] =
        rand () % (w - 2) + 1 ;
      buah [1] = 
        rand () % (h - 2) + 1 ;
    }
    temp = temp->next ;
  }
  return buah ;
}

ulr *ekor (ulr *head,
           int  h,
           int  w)
{
  ulr *temp = (ulr *)
    malloc (sizeof (ulr)) ;
  if (head != NULL)
  {
    temp->arah = head->arah ;
    switch (head->arah)
    {
      case kanan :
        temp->x = head->x - 1 ;
        temp->y = head->y ;
        break ;
      case kiri :
        temp->x = head->x + 1 ;
        temp->y = head->y ;
        break ;
      case atas :
        temp->x = head->x ;
        temp->y = head->y + 1 ;
        break ;
      case bawah :
        temp->x = head->x ;
        temp->y = head->y - 1 ;
        break ;
    }
    temp->next = head ;
  } 
  else
  {
    temp->arah = kanan ;
    temp->x = w / 2 ;
    temp->y = h / 2 ;
    temp->next = NULL ;
  }
  return temp ;
}

void hapus (ulr *head)
{
  ulr *temp ;
  while (head != NULL)
  {
    temp = head ;
    head = head->next ;
    free (temp) ;
  }
}

int nabrak (ulr *head,
            ulr *pala,
            int  h,
            int  w)
{
  ulr *temp = head ;
  while (temp != pala)
  {
    if (
      temp->x == pala->x &&
      temp->y == pala->y ||
      pala->x == 0 ||
      pala->x == w - 1 ||
      pala->y == 0 ||
      pala->y == h - 1
    )
      return 1 ;
    temp = temp->next ;
  }
  return 0 ;
}

void mulai (int u,
            int h,
            int w,
            int c)
{
  int  i ;
  mde  mode = awal ;
  int  play = 1 ;
  int  score = 0 ;
  int  count = 0 ;
  
  ulr *head =
    ekor (NULL, h, w) ;
  ulr *pala = head ;
  for (i = 0 ; i < u - 1 ; i++)
    head = ekor (head, h, w) ;
  int *buah =
    berbuah (head, h, w) ;
  wrn *papan =
    plot (head, buah, h, w) ;
  cetak (
    papan, score, h, w, c, mode
  ) ;
  
  mode = maen ;
  getch () ;
  do
  {
    if (kbhit ())
    {
      switch (getch ())
      {
        case 77 :
          if (
            pala->arah != kiri
          )
            pala->arah = kanan ;
          break ;
        case 75 :
          if (
            pala->arah != kanan
          )
            pala->arah = kiri ;
          break ;
        case 72 :
          if (
            pala->arah != bawah
          )
            pala->arah = atas ;
          break ;
        case 80 :
          if (
            pala->arah != atas
          )
            pala->arah = bawah ;
          break ;
        case 112 :
          up (
            (h % 2) ? h / 2 +
            2 : h / 2 + 1
          ) ;
          cetak (
            papan, score,
            h, w, c, pause
          ) ;
          getch () ;
          break ;
        case 27 :
          play = 0 ;
          mode = gameover ;
          break ;
      }
    }
    
    up (
      (h % 2) ? h / 2 + 2 :
      h / 2 + 1
    ) ;
    papan = 
      plot (head, buah, h, w) ;
    
    if (count)
    {
      jalan (head) ;
    
      if (
        pala->x == buah [0] &&
        pala->y == buah [1]
      )
      {
        head = ekor
          (head, h, w) ;
        score += 10 ;
        buah =
          berbuah (head, h, w) ;
      }
      if (
        nabrak (head, pala,
        h, w)
      )
      {
        play = 0 ;
        mode = gameover ;
      }
      
      count = 0 ;
    }
    else count++ ;
    
    cetak (
      papan, score,
      h, w, c, mode
    ) ;
    
    Sleep (50) ;
  }
  while (play) ;
  hapus (head) ;
  free (papan) ;
}

/*-=----=----=----=----=----=-*/

void uler (int u,
           int h,
           int w)
{
  int c = inisiasi () ;
  mulai (u, h, w, c) ;
  terminasi (c) ;
}