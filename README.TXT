/*-=----=----=----=----=----=-*
 *   __   __                  *
 *  / /  / /\___  __          *
 * / /  / / / _ \/*_\         *
 * \ \_/ / / /__/ /           *
 *  \___/_/\___/ /            *
 * /_ \_/  \/ __/             *
 *   \___/\__/ v1.0.0-beta    *
 *                            *
 *        (c) 2018 NizhamPihe *
 *-=----=----=----=----=----=-*/

INFORMATION
===========
  Nama App  = Uler
  Nama File = ULER.C
  Versi     = v1.0.0-beta
  Categori  = GAME
  Genre     = ARCADE
  Platform  = WINDOWS CONSOLE
  Ukuran    = 10399 bytes
  Baris     = 596 lines
  
DESCRIPTION
===========
    Program Game Snake
  Sederhana

GAMEPLAY
========

  HOW TO PLAY
  ===========
    - Ambil buah untuk
      mendapatkan score
    - Semakin banyak buah
      yang diambil tubuh
      ular semakin panjang
    - jangan sampai menabrak
      tembok
    - jangan sampai memakan
      diri sendiri
  
  CONTROL
  =======
    - Panah : Jalan
    - P     : Pause
    - Esc   : Keluar
  
  FEATURES
  ========
    - Pause Button


INSTALATION
===========
  gcc -o ULER.EXE -c ULER.C

COPYRIGHT
=========
  Copyright (c) 2018, NizhamPihe
  Released Under The MIT License